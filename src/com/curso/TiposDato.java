package com.curso;

public class TiposDato{
	
	int i;
	
	public static void main(String[] args) {
		
		{
			int i = 5;
		}

		int i = 0;
		
		System.out.println("i es: " +  i);

		byte b1 = 1;

		byte b2 = 127;

		byte b3 = (byte) 128;

		System.out.println("byte 128 con cast: " + b3);

		short s1 = b1;

		// b1 = s1;

		b1 = (byte) (b1 + 1);

		b1 += 1 + 1; //b1 = (byte) (b1 + (1+1));

		b1 ++; //b1 = (byte) (b1 + 1);

		float f1 = 1.2F;

		double d1 = 2.3;

		// literales que son considerados keywords: null, true, false
		
		double l1 = 1e-2;

		System.out.println("1e-2: " + l1);

		float f2 = 1e-20f;

		long l3 = 1000_000_000L;

		long _1000_000_000 = 1;

		long l4 = _1000_000_000;
		
		long l5 = 1000_000_000;

		int i4 = 10;

		int i5 = 0x000F;

		int i6 = 010;

		int i7 = 0x010;

		int i8 = 10;

		int i9 = 010;

		int i10 = 0b1_0010_1101_1000_1101_0101_1011;

		// System.out.println("i10: " + i10);
	}
}