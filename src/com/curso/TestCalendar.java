package com.curso;

import java.util.Calendar;

/**
 * La clase Test Calendar muestra un calendario tipo comando cal en linux, pasándole el mes y año a mostrar.
 * @author Zeuxis Villegas
 * @version 13/08/2018
 */
public class TestCalendar {
	/**
     * @param args aquí se obtiene el valor del mes y año a mostrar por el calendario, formato mm aaaa.
     */
	public static void main(String[] args) {
		if (args.length == 2) {
			int anio = Integer.parseInt(args[0]); 

			int mes = Integer.parseInt(args[1]);


			//Validar que se hayan proporcionado 2 argumentos
			if(args.length != 2) {
				System.out.println("Uso correcto: java TestCalendar anio mes");
			}
			//Convertir los argumentos en enteros

			//Crear un calendar con los valores proporcionados

			Calendar c = Calendar.getInstance();
			c.set(anio, mes, 1);
			System.out.println(c.getTime());

			System.out.println("First week day: " + c.get(Calendar.DAY_OF_WEEK));
			System.out.println("Last day of month: " + c.getActualMaximum(Calendar.DAY_OF_MONTH));

			//Iniciar ciclo de impresion de numeros

			System.out.printf("Año: Mes: %n", anio, mes);
			System.out.println("Do Lu Ma Mi Ju Vi Sa");
			System.out.println("--------------------");

			for (int i = 1; i<= 7 ; i++) 
				if (i < c.get(Calendar.DAY_OF_WEEK))
					System.out.print("   ");
			for (int i = 1; i <= c.getActualMaximum(Calendar.DAY_OF_MONTH) ; i++) {
				System.out.printf("%3d", i);
				if ((i + (c.get(Calendar.DAY_OF_WEEK) - 1)) % 7 == 0)
					System.out.println();
			}
			System.out.println();
		}else{
			System.out.println("Se debe de ingresar el año y el número de mes (en ese orden y separados por un espacio) al ejecutar el programa.");
		}
	}
}