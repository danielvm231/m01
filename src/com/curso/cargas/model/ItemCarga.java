package com.curso.cargas.model;

import com.curso.model.ItemBean;

public class ItemCarga extends ItemBean{
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public java.lang.String toString() {
        /*return super.toString() + "ItemCarga{" +
                "status=" + status +
                '}';*/
        return "Item{" +
                "itemID=" + this.getItemID() +
                ", desc='" + desc + '\'' +
                ", price=" + this.getPrice() +
                ", quantity=" + this.getQuantity() +
                '}';
    }
}