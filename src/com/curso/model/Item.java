package com.curso.model;

/**
* Clase que contiene los productos de una venta
* @author zeuxis.villegas
*/
public class Item{
	private int itemID;
	private String desc;
	private double price;
	private int quantity;

	/*Constructores*/

    public Item(int itemID, String desc, double price, int quantity) {
        this.itemID = itemID;
        this.desc = desc;
        this.price = price;
        this.quantity = quantity;
    }

    /*Métodos de negocio*/
	public void updateItem(){

	}

	public void finishStock(){

	}

	private void init(){

	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}