package com.curso.model;

import java.io.Serializable;
/**
* Clase que contiene los productos de una venta
* @author zeuxis.villegas
*/
public class ItemBean implements Serializable{
	private int itemID;
	protected String desc;
	private double price;
	private int quantity;

	/*Constructores*/

    public ItemBean(){

    }

    /*Métodos de negocio*/

	public void updateItem(){

    }

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

    @Override
    public java.lang.String toString() {
        return "ItemDTO{" +
                "itemID=" + itemID +
                ", desc='" + desc + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}