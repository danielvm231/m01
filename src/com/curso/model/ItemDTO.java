package com.curso.model;

/**
* Clase que contiene los productos de una venta
* @author zeuxis.villegas
*/
public class ItemDTO implements Serializable{
	private int itemID;
	private String desc;
	private double price;
	private int quantity;

	/*Constructores*/

    public ItemDTO(int itemID, String desc, double price, int quantity) {
        this.itemID = itemID;
        this.desc = desc;
        this.price = price;
        this.quantity = quantity;
    }

    public ItemDTO(){

    }

    /*Métodos de negocio*/

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

    @Override
    public java.lang.String toString() {
        return "ItemDTO{" +
                "itemID=" + itemID +
                ", desc='" + desc + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}