package com.curso;

public class TestArrays{
	public static void main(String[] args) {
		// inicialización en 3 pasos
		// declaración
		int[] array = null;
		// asignación de espacio
		array = new int[3];
		array = new int[]{1,2,3,4,5,6};
		// asignación de valores
		array[0] = 100;

		// inicialización en 1 paso
		int array2[] = {1, 2, 3, 4, 5, 6};
		int array3[] = new int[]{1, 2, 3, 4, 5, 6};

		// int array4[5];   ----> incorrecto
		int array5[] = null;

		// array5 = new int[5]{1,2,3};   ----> incorrecto
		// array5 = {1,2,3};   ----> incorrecto

		int[] array7[] = {};
		int[] array8[] = {{}, {}};
		int[] array9[] = {{}, null, {1, 2, 3}};

		String [] array10, array11;
		array10 = new String[]{"Hola", "Mundo", "!"};
		array11 = new String[3];
		array11[2] = array10[2];

		String array12[] = {"Hola", "Mundo", "!"};
		String array32[] = new String[]{"Hola", "Mundo", "!", null};

		String [] array14[] = {};
		String [] array15[] = {{}, null};
		String [] array16[] = {null, {null}, {"Homero"}};


		String []array17[] = {{"1"}, null};


		char cA[][] = new char[3][];
		for (int i=0; i<cA.length; i++)
			cA[i] = new char[4];
	}
}